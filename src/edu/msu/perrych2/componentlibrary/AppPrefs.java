/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.msu.perrych2.componentlibrary;

/**
 *
 * @author Christopher Perry <perrrych2@msu.edu>
 */
public enum AppPrefs {
  MAIN_WINDOW_X,
  MAIN_WINDOW_Y,
  MAIN_WINDOW_WIDTH,
  MAIN_WINDOW_HEIGHT,
  FILE_PATH
}
