/*
 * Copyright 2017, Christopher C. Perry
 */
package edu.msu.perrych2.componentlibrary;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import javax.swing.JComponent;

/**
 *
 * @author Christopher Perry <perrrych2@msu.edu>
 */
public class AutofitImage extends JComponent {
  private Image _image;
  private Image _workingImage = null;
  private int _xOffset = 0;
  private int _yOffset = 0;
  private final Graphics2DState _gstate = new Graphics2DState();
  
  public AutofitImage() {
    _image = null;
    addComponentListener(new ComponentAdapter() {
      @Override
      public void componentResized(ComponentEvent e) {
          resizeImage();
      }
    });
  }
  public String getImageCredits() {
    return "Sample imagery courtesy of publicdomainarchive.com.  "
        + "Icons courtesy of openclipart.org.";
  }
  public void setImage(Image image) {
    _image = image;
    resizeImage();
    invalidate();
  }
  
  public Image getImage() {
    return _image;
  }
  
  public void setImageFromFile(File source) throws IOException {
    _image = ImageIO.read(source);
    resizeImage();
    invalidate();
  }
  
  @Override
  public void paint(Graphics g) {
    Graphics2D g2 = (Graphics2D)g;
    _gstate.save(g2);
    paint2D(g2);
    _gstate.restore(g2);
  }
  
  private void initializeImage() {
    String imagepath = String.format("resources/images/stock-%02d.jpg", 
              (new Random()).nextInt(7) + 1
          );
    try {
      _image = ImageIO.read(getClass().getResourceAsStream(imagepath));
      resizeImage();
    } catch (IOException e) {
      java.util.logging.Logger.getLogger(getClass().getName()).log(
          Level.WARNING, "Could not load stock image: {0}", imagepath
      );
    }
  }
  
  private void paint2D(Graphics2D g2) {
    if(_workingImage == null) resizeImage();
    g2.setColor(getBackground());
    g2.fillRect(0, 0, getWidth(), getHeight());
    g2.drawImage(_workingImage, _xOffset, _yOffset, this);
  }
  
  private void resizeImage() {
    if(_image == null) {
      initializeImage();
      return;
    }
    Dimension d = getScaledDimensions(
        getWidth(), getHeight(),
        _image.getWidth(this), _image.getHeight(this)
    );
    if(d.getWidth() > 0f && d.getHeight() > 0f) {
      _workingImage = _image.getScaledInstance(
          d.width, d.height, Image.SCALE_SMOOTH
      );
    }
    if(_workingImage != null) {
      double xPadding = (double)getWidth() - (double)_workingImage.getWidth(null);
      double yPadding = (double)getHeight() - (double)_workingImage.getHeight(null);
      _xOffset = (int)Math.round(xPadding * getAlignmentX());
      _yOffset = (int)Math.round(yPadding * getAlignmentY());
    }
  }
  
  private Dimension getScaledDimensions(
          int canvasWidth, int canvasHeight, int imageWidth, int imageHeight) {
    double scale = Math.min(
            (double)canvasWidth / (double)imageWidth,
            (double)canvasHeight / (double)imageHeight
    );
    return new Dimension((int)(imageWidth * scale), (int)(imageHeight * scale));
  } 
  
  @Override
  public boolean imageUpdate(
      Image img, int infoflags, 
      int x, int y, 
      int width, int height
  ) {
    boolean returnvalue = super.imageUpdate(img, infoflags, x, y, width, height);
    repaint();
    return returnvalue;
  }
}
