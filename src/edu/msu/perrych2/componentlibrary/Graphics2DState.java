/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.msu.perrych2.componentlibrary;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;

/**
 *
 * @author Christopher Perry <perrrych2@msu.edu>
 */
public class Graphics2DState {
  private Color _color = null;
  private Stroke _stroke = null;
  private AffineTransform _transform = null;

  public void save(Graphics2D g) {
    _color = g.getColor();
    _stroke = g.getStroke();
    _transform = g.getTransform();
  }  
  
  public void restore(Graphics2D g) {
    if(_color != null) g.setColor(_color);
    if(_stroke != null) g.setStroke(_stroke);
    if(_transform != null) g.setTransform(_transform);
  }
}
